$("#action").click(function(){
        $.ajax ({
            url: 'cache/champions.json',
            dataType:'json',
            data: "test=1",
            method: 'GET'
        }).done(function(start) {
            $('#info').append('<p>' +  start['type']  + '</p>')
            $('#info').append('<p>' +  start['version']  + '</p>')
            for(name in start.data){
                $('#text').append('<div class="col-4" id='+ name + '><h2>' +  name  + '</h2</div>')
                for(i in start.data[name]){
                    if(i == "image"){
                        $('#' + name).append("<img src='https://ddragon.leagueoflegends.com/cdn/7.10.1/img/champion/" +start.data[name].image['full']+ "'>")
                    }
                    else{
                    $('#' + name).append("<p>" + i + ':' + start.data[name][i]+ "</p>")
                    }
                }
            }
        })
    });
/*
Résultat actuel :
Jax

0

1

2
Sona

0

1

2

3
Tristana

0

1

2

3

4

5

6

7
Varus
*/
