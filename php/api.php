<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Tuto PHP API</title>
</head>
<body>
<?php

function cleanString($string) {
    // on supprime : majuscules ; / ? : @ & = + $ , . ! ~ * ( ) les espaces multiples et les underscore
    $string = strtolower($string);
    $string = preg_replace("/[^a-z0-9_'\s-]/", "", $string);
    $string = preg_replace("/[\s-]+/", " ", $string);
    $string = preg_replace("/[\s_]/", " ", $string);
    return $string;
}
function apijson(){
    $dir = '../cache';
    $url = "https://euw1.api.riotgames.com/lol/static-data/v3/champions?locale=fr_FR&champListData=image&dataById=true"."&api_key=RGAPI-e9ebe59e-d33d-43b8-a000-ff0a408f4751";
    $raw = file_get_contents($url);
    file_put_contents($dir . '/champions.json', $raw);
    $json = json_decode($raw);
}
apijson();


?>
</body>
<html>
